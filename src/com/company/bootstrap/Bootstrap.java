package com.company.bootstrap;

import com.company.advanced.Binary;
import com.company.advanced.Linear;
import com.company.advanced.Search;

import java.util.Scanner;

public class Bootstrap {

    Linear linear = new Linear();
    Binary binary = new Binary();

    public void run() {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите цифру которую хотите найти: ");
        if (in.hasNextInt()) {
            int userNumber = in.nextInt();
            init(userNumber);
        } else {
            print("Необходимо вводить только числа");
        }
    }

    private void init(int userNumber) {
        int[] sortArrayNumber = new int[]{1, 33, 54, 55, 77};
        findElement(linear, sortArrayNumber, userNumber);
        findElement(binary, sortArrayNumber, userNumber);
    }

    private void findElement(Search search, int[] source, int userNumber) {
        if (source.length == 0) {
            print("Массив не должен быть пустым");
            return;
        }
        int index = search.findIndexElement(source, userNumber);
        String message = index != -1 ? String.format("Число %d найдено на позиции: %d ", userNumber, index) : String.valueOf(index);
        print(message);
    }

    private void print(String message) {
        System.out.println(message);
    }

}

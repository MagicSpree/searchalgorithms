package com.company.advanced;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Linear implements Search {

    @Override
    public int findIndexElement(int[] source, int elementToSearch) {

        for (int index = 0; index < source.length; index++) {
            if (source[index] == elementToSearch)
                return index;
        }
        return -1;
    }
}

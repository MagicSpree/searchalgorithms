package com.company.advanced;

public class Binary implements Search {

    @Override
    public int findIndexElement(int[] source, int elementToSearch) {
        int firstIndex = 0;
        int lastIndex = source.length - 1;

        while (firstIndex <= lastIndex) {
            int middleIndex = (firstIndex + lastIndex) / 2;

            if (source[middleIndex] == elementToSearch) {
                return middleIndex;
            } else if (source[middleIndex] < elementToSearch)
                firstIndex = middleIndex + 1;

            else if (source[middleIndex] > elementToSearch)
                lastIndex = middleIndex - 1;

        }
        return -1;
    }
}

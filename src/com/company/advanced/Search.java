package com.company.advanced;

public interface Search {
    int findIndexElement(int[] source, int elementToSearch);
}
